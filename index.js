const fs = require('fs')
const colors = require('colors');


const grabAllValuesFromObjectInCSV = (myObj) => {
   let csvString = '';
    Object.keys(myObj).forEach((x, index) => {
        if (index !== 0) csvString += ','
        csvString += `'${myObj[x].replace(/\'/g, '\'\'')}'`
    });
    return csvString;
}

const generateInsertStatements = (jsonData) => {
    let statement = '';
    jsonData.forEach(x => {
        statement += `INSERT INTO ${process.argv[4]} (${Object.keys(x).join()})
VALUES
(${grabAllValuesFromObjectInCSV(x)})\n`
    });

    return statement
};

if (process.argv.length < 3) {
    console.error('Please supply an object to parse through'.red);
    return
}

fs.readFile(process.argv[2], 'utf8' , (err, data) => {
  if (err) {
      console.error(err.red)
    return
  }
    fs.writeFile(process.argv[3], generateInsertStatements(JSON.parse(data)), err => {
    if (err) {
        console.error(err.red)
        return
    }
    //file written successfully
        console.info('File written successfully'.green);
    })

})


