# Instructions for use
```bash
npm run start {nameOfJsonFile} {nameOfNewSQLFile} {tableName}
```

So the command will look like this:
```bash
npm run start emailTemplate.json emailTemplate.sql EmailTemplates
```

This will generate a create statement for SQL Server. I created this because I found it useful when I was pulling some data down in JSON format, and wanted to upload it into sql server without needing to createa new api call to handle the JSON data.
